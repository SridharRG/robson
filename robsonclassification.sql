CREATE DATABASE  IF NOT EXISTS `robsonclassification`;

USE `robsonclassification`;

CREATE TABLE `robsonsdata` (
  `id` int NOT NULL AUTO_INCREMENT,
  `obs_index` varchar(255) DEFAULT NULL,
  `weeks` varchar(255) DEFAULT NULL,
  `pog` varchar(255) DEFAULT NULL,
  `previous_cesarean` varchar(255) DEFAULT NULL,
  `fetus_type` varchar(255) DEFAULT NULL,
  `presentation_single` varchar(255) DEFAULT NULL,
  `presentation_twin` varchar(255) DEFAULT NULL,
  `Labour` varchar(255) DEFAULT NULL,
  `ripening` varchar(255) DEFAULT NULL,
  `induced_augmented` varchar(255) DEFAULT NULL,
  `delivery` varchar(255) DEFAULT NULL,
  `indication_ovd` varchar(255) DEFAULT NULL,
  `indication_cesarean` varchar(255) DEFAULT NULL,
  `Stage` varchar(255) DEFAULT NULL,
  `B1Gender` varchar(255) DEFAULT NULL,
  `B1Weight` varchar(255) DEFAULT NULL,
  `b1_date_of_birth` date DEFAULT NULL,
  `b1_time_of_birth` time DEFAULT NULL,
  `B2Gender` varchar(255) DEFAULT NULL,
  `B2Weight` varchar(255) DEFAULT NULL,
  `b2_date_of_birth` date DEFAULT NULL,
  `b2_time_of_birth` time DEFAULT NULL,
  `b1apgar1` varchar(255) DEFAULT NULL,
  `b1apgar5` varchar(255) DEFAULT NULL,
  `b1outcome` varchar(255) DEFAULT NULL,
  `b2apgar1` varchar(255) DEFAULT NULL,
  `b2apgar5` varchar(255) DEFAULT NULL,
  `b2outcome` varchar(255) DEFAULT NULL,
  `indication` varchar(255) DEFAULT NULL,
  `b1final_outcome` varchar(255) DEFAULT NULL,
  `b2final_outcome` varchar(255) DEFAULT NULL,
  `indication_for_induction` varchar(255) DEFAULT NULL,
  `created_by` enum('student','doctor','department') DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `review` varchar(255) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  `patient_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `patient_id` varchar(50) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `groups_ibfk_1` (`patient_id`),
  CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `robsonsdata` (`patient_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `loginauth` (
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role`enum('doctor','student','department') NOT NULL,
  `department` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;