import mysql from 'mysql2';

export const con = mysql.createConnection({
  host: process.env.DB_HOST || 'mysql-robson',
  user: process.env.DB_USER || 'root',
  password: process.env.DB_PASSWORD || 'robsonclassification',
  database: process.env.DB_NAME || 'robsonclassification'
});

con.connect((err) => {
  if (err) console.log('connection failed', err);
  else console.log('connection successful');
});
